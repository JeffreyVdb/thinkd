#ifndef _THINKD_H_
#define _THINKD_H_

#define THINKD_PIDFILE "/var/run/thinkd.pid"
#define THINKD_LOCKFILE "/var/lock/thinkd"

#define AC_SLEEP_TIME 3
#define BAT_SLEEP_TIME 15

#define DAEMON_NAME "thinkd"

#endif /* _THINKD_H_ */
